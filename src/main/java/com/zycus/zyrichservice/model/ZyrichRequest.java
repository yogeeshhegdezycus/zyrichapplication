package com.zycus.zyrichservice.model;

public class ZyrichRequest {

	private String userName;

	private String password;
	
	private String jobName;
	
	private String userProjectTable;
	
	private String repository;
	
	private String whereCondition;
	
	private String modules;
	
	private String reqColumnName;
	
	private String primeryKey;
	
	private String fields;
	
	private String jbpmId;
	
	private String projectName;
	
	
	
	

	public String getDisplayName() {
		return jobName+"_"+jbpmId+"_"+projectName;
	}


	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getJbpmId() {
		return jbpmId;
	}

	public void setJbpmId(String jbpmId) {
		this.jbpmId = jbpmId;
	}

	public String getFields() {
		return fields;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	public String getReqColumnName() {
		return reqColumnName;
	}

	public void setReqColumnName(String reqColumnName) {
		this.reqColumnName = reqColumnName;
	}

	public String getPrimeryKey() {
		return primeryKey;
	}

	public void setPrimeryKey(String primeryKey) {
		this.primeryKey = primeryKey;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getUserProjectTable() {
		return userProjectTable;
	}

	public void setUserProjectTable(String userProjectTable) {
		this.userProjectTable = userProjectTable;
	}

	public String getRepository() {
		return repository;
	}

	public void setRepository(String repository) {
		this.repository = repository;
	}

	public String getWhereCondition() {
		return whereCondition;
	}

	public void setWhereCondition(String whereCondition) {
		this.whereCondition = whereCondition;
	}

	public String getModules() {
		return modules;
	}

	public void setModules(String modules) {
		this.modules = modules;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "ZyrichRequest [userName=" + userName + ", password=" + password
				+ ", jobName=" + jobName + ", userProjectTable="
				+ userProjectTable + ", repository=" + repository
				+ ", whereCondition=" + whereCondition + ", modules=" + modules
				+ "]";
	}

	
	
	
	
	
	
}
