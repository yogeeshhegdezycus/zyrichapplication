package com.zycus.zyrichservice.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.zycus.zyrich.atoms.IndexConfig;
import com.zycus.zyrich.atoms.IndexTableMetaData;
import com.zycus.zyrich.atoms.JobConfig;
import com.zycus.zyrich.atoms.Tag;
import com.zycus.zyrich.atoms.User;
import com.zycus.zyrich.dao.IndexConfigDAO;
import com.zycus.zyrich.dao.StatusTableDAO;
import com.zycus.zyrich.dao.UserAuthenticationDAO;
import com.zycus.zyrich.exceptions.DatabaseConnectionException;
import com.zycus.zyrich.exceptions.ResourceException;
import com.zycus.zyrich.exceptions.ZyrichException;
import com.zycus.zyrich.utils.ConnectionPoolHandler;
import com.zycus.zyrichservice.management.SearchTaskJBPM;
import com.zycus.zyrichservice.model.RequestColumnName;
import com.zycus.zyrichservice.model.ZyrichRequest;

public class ZyrichProcessRequest {

	private static Logger  logger = Logger.getLogger(ZyrichProcessRequest.class);
	
	private StringBuilder errorList = new StringBuilder();
			
	public ZyrichProcessRequest(){
		
	}
	
	public Map<String,String> startProcessingForSuppRisk(ZyrichRequest zyRequest) throws Exception{
		
		try {
			
			zyRequest.setJobName("ZY");
//			User user =doValidation(zyRequest);
			
			Map<String,String> columnMapping = new HashMap<>();
			Set<String> set = new HashSet<>();
			HashMap<String,String> map = new HashMap<>();
			JobConfig jobConfig = new JobConfig(); 
			
			logger.info("***********************JOBNAME*****************************");
			logger.info("                                       "+zyRequest.getJobName()+"                       ");
			logger.info("************************************************************");
			
			jobConfig.setIndexName(zyRequest.getRepository());
			jobConfig.setJobName(zyRequest.getJobName());
//			jobConfig.setUserName(user.getLoginname());
			jobConfig.setWhereCondition(zyRequest.getWhereCondition());
//			jobConfig.setUserDbConnDetails(new UserAuthenticationDAO()
//								.getUserDbConnDetails(user));
			
			String tags[]=null;
			String fields[] =null;

			if(zyRequest.getModules()!=null&&!zyRequest.getModules().trim().equals(""))
			tags=zyRequest.getModules().split(",");
			if(zyRequest.getFields()!=null&&!zyRequest.getFields().trim().equals(""))
				fields=zyRequest.getFields().split(",");

			if(fields!=null){
				logger.info("fields are :");
				
				for(int i=0;i<fields.length;i++){
					fields[i]=fields[i].trim();
					logger.info(fields[i]);
				}
			}

			if (fields != null)
				set.addAll(Arrays.asList(fields));


			if(tags!=null){
				logger.info("tags are :");
				for(int i=0;i<tags.length;i++){
					tags[i]=tags[i].trim();
					logger.info(tags[i]);
				}
			}
			
			
//			IndexConfigDAO indexConfigDAO = new IndexConfigDAO();
			HashMap<String, String[]> hashMap = new HashMap<String, String[]>();
//			for (Tag tag : indexConfigDAO.getTags(jobConfig.getIndexName())) {
//				hashMap.put(tag.getTagName().toUpperCase(), tag.getFields());
//				logger.info("map tag name: "+tag.getTagName());
//			}
			if (tags != null) {

				for (String tag : tags) {
					if (tag.equalsIgnoreCase("custom")) {
					} else {

						String[] list = hashMap.get(tag.toUpperCase());
						for (int i = 0; i < list.length; i++)
							set.add(list[i]);
					}
				}
			}
			
			
			jobConfig.setFieldsToPopulate(Arrays.asList(set.toArray(new String[0])));
			
			
			if(zyRequest.getReqColumnName()!=null){
				
				String reqColumnArr[] = zyRequest.getReqColumnName().split(",");
				
				for(String entry: reqColumnArr){
					logger.info("before : "+entry.substring(0, entry.indexOf(":"))+" | After : "+entry.substring(entry.indexOf(":")+1,entry.length()));
					columnMapping.put(entry.substring(0, entry.indexOf(":")), entry.substring(entry.indexOf(":")+1,entry.length()));
				}
			}
			
			StringBuffer reqColumnName = new StringBuffer();
			String reqColumnlblName=RequestColumnName.getColumnNames(zyRequest.getRepository());
//			indexConfigDAO.getColumnLableNames(zyRequest.getRepository());
			for(String columnName:reqColumnlblName.split(",")){
				logger.info("for each: "+columnName);
				if(columnMapping.get(columnName)==null||columnMapping.get(columnName).trim().equals("")||columnMapping.get(columnName).toLowerCase().trim().equals("null")){
					reqColumnName.append("null,");
				}else reqColumnName.append(columnMapping.get(columnName).trim().toUpperCase()+",");
			}
			
			reqColumnName.deleteCharAt(reqColumnName.length()-1);
			
			
			
			logger.info("reqColumnlblName :"+reqColumnlblName);
			logger.info("reqColumnName :"+reqColumnName.toString());
			logger.info("jobtable :"+zyRequest.getUserProjectTable());
			logger.info("primeryKey :"+zyRequest.getPrimeryKey());
			logger.info("indexname: "+jobConfig.getIndexName());
			logger.info("Fields to populate: "+jobConfig.getFieldsToPopulate().toString());
			
			map.put("reqColumnlblName", reqColumnlblName);
			map.put("reqColumnName", reqColumnName.toString());
			map.put("jobtable",zyRequest.getUserProjectTable());
			map.put("primeryKey", zyRequest.getPrimeryKey());
			map.put("indexname", jobConfig.getIndexName());
			jobConfig.setMetaData(new IndexTableMetaData(map));

//			IndexConfig indexConfig = new IndexConfigDAO()
//			.getIndex(map);
			IndexConfig indexConfig = RequestColumnName.getIndexConfig(map);
//TODO: user is null
			SearchTaskJBPM searchTask = new SearchTaskJBPM(indexConfig, jobConfig, null);
			
//			new StatusTableDAO().registerNewJobToStatusTableNew(jobConfig,zyRequest.getDisplayName());
//			new StatusTableDAO().updateState(zyRequest.getDisplayName(),
//					jobConfig.getUserName(), "ACTIVE");
			return searchTask.runNewForSupplier(zyRequest);

		}  catch (Exception e) {
			e.printStackTrace();
			logger.error(e);

				return null;
		}
	
	}
	
	public int startProcessing(ZyrichRequest zyRequest) throws Exception{
			
		try {
			
			zyRequest.setJobName("ZY");
			User user =doValidation(zyRequest);
			
			Map<String,String> columnMapping = new HashMap<>();
			Set<String> set = new HashSet<>();
			HashMap<String,String> map = new HashMap<>();
			JobConfig jobConfig = new JobConfig(); 
			
			logger.info("***********************JOBNAME*****************************");
			logger.info("                                       "+zyRequest.getJobName()+"                       ");
			logger.info("************************************************************");
			
			jobConfig.setIndexName(zyRequest.getRepository());
			jobConfig.setJobName(zyRequest.getJobName());
			jobConfig.setUserName(user.getLoginname());
			jobConfig.setWhereCondition(zyRequest.getWhereCondition());
			jobConfig.setUserDbConnDetails(new UserAuthenticationDAO()
								.getUserDbConnDetails(user));
			
			String tags[]=null;
			String fields[] =null;

			if(zyRequest.getModules()!=null&&!zyRequest.getModules().trim().equals(""))
			tags=zyRequest.getModules().split(",");
			if(zyRequest.getFields()!=null&&!zyRequest.getFields().trim().equals(""))
				fields=zyRequest.getFields().split(",");

			if(fields!=null){
				logger.info("fields are :");
				
				for(int i=0;i<fields.length;i++){
					fields[i]=fields[i].trim();
					logger.info(fields[i]);
				}
			}

			if (fields != null)
				set.addAll(Arrays.asList(fields));


			if(tags!=null){
				logger.info("tags are :");
				for(int i=0;i<tags.length;i++){
					tags[i]=tags[i].trim();
					logger.info(tags[i]);
				}
			}
			
			
			IndexConfigDAO indexConfigDAO = new IndexConfigDAO();
			HashMap<String, String[]> hashMap = new HashMap<String, String[]>();
			for (Tag tag : indexConfigDAO.getTags(jobConfig.getIndexName())) {
				hashMap.put(tag.getTagName().toUpperCase(), tag.getFields());
				logger.info("map tag name: "+tag.getTagName());
			}
			if (tags != null) {

				for (String tag : tags) {
					if (tag.equalsIgnoreCase("custom")) {
					} else {

						String[] list = hashMap.get(tag.toUpperCase());
						for (int i = 0; i < list.length; i++)
							set.add(list[i]);
					}
				}
			}
			
			
			jobConfig.setFieldsToPopulate(Arrays.asList(set.toArray(new String[0])));
			
			
			if(zyRequest.getReqColumnName()!=null){
				
				String reqColumnArr[] = zyRequest.getReqColumnName().split(",");
				
				for(String entry: reqColumnArr){
					logger.info("before : "+entry.substring(0, entry.indexOf(":"))+" | After : "+entry.substring(entry.indexOf(":")+1,entry.length()));
					columnMapping.put(entry.substring(0, entry.indexOf(":")), entry.substring(entry.indexOf(":")+1,entry.length()));
				}
			}
			
			StringBuffer reqColumnName = new StringBuffer();
			String reqColumnlblName=indexConfigDAO.getColumnLableNames(zyRequest.getRepository());
			for(String columnName:reqColumnlblName.split(",")){
				logger.info("for each: "+columnName);
				if(columnMapping.get(columnName)==null||columnMapping.get(columnName).trim().equals("")||columnMapping.get(columnName).toLowerCase().trim().equals("null")){
					reqColumnName.append("null,");
				}else reqColumnName.append(columnMapping.get(columnName).trim().toUpperCase()+",");
			}
			
			reqColumnName.deleteCharAt(reqColumnName.length()-1);
			
			
			
			logger.info("reqColumnlblName :"+reqColumnlblName);
			logger.info("reqColumnName :"+reqColumnName.toString());
			logger.info("jobtable :"+zyRequest.getUserProjectTable());
			logger.info("primeryKey :"+zyRequest.getPrimeryKey());
			logger.info("indexname: "+jobConfig.getIndexName());
			logger.info("Fields to populate: "+jobConfig.getFieldsToPopulate().toString());
			
			map.put("reqColumnlblName", reqColumnlblName);
			map.put("reqColumnName", reqColumnName.toString());
			map.put("jobtable",zyRequest.getUserProjectTable());
			map.put("primeryKey", zyRequest.getPrimeryKey());
			map.put("indexname", jobConfig.getIndexName());
			jobConfig.setMetaData(new IndexTableMetaData(map));

			IndexConfig indexConfig = new IndexConfigDAO()
			.getIndex(map);

			SearchTaskJBPM searchTask = new SearchTaskJBPM(indexConfig, jobConfig, user);
			
			new StatusTableDAO().registerNewJobToStatusTableNew(jobConfig,zyRequest.getDisplayName());
			new StatusTableDAO().updateState(zyRequest.getDisplayName(),
					jobConfig.getUserName(), "ACTIVE");
			searchTask.runNew(zyRequest);
			
			return 0;

		}  catch (Exception e) {
			e.printStackTrace();
			logger.error(e);

				return 1;
		}
	
	}
	
	
	
	
	private User doValidation(ZyrichRequest zyRequest) throws Exception{
		User user = new User();
		user.setLoginname(zyRequest.getUserName());
		user.setPassword(zyRequest.getPassword());
		String result="";
		try {
			user = new UserAuthenticationDAO().validate(user);
			if(user==null){
				logger.error("ZYRICH USER VALIDATION FAILED!!");				
				throw new RuntimeException("ZYRICH USER VALIDATION FAILED!!");
			}

			} catch (ZyrichException e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
		}

		try {
			IndexConfigDAO indexConfigDAO = new IndexConfigDAO();
			boolean nameExists=false;
			for(String tempIndexName:indexConfigDAO.getIndexNames()){
				if(tempIndexName.equals(zyRequest.getRepository()))nameExists=true;
			}
			if(!nameExists){
				logger.error("ZYRICH REPOSITORY/INDEX NAME PROVIDED DOES NOT EXIST!!");				
				//TODO: throw exception instead of returning void;
				throw new RuntimeException("ZYRICH REPOSITORY/INDEX NAME PROVIDED DOES NOT EXIST!!");
			}

			} catch (ZyrichException e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
		}

		Connection authCon=null;
		Connection userCon =null;
		Connection appCon=null;
		
		try {
			ConnectionPoolHandler pool=new ConnectionPoolHandler();
			authCon=pool.getConnectionfromPool("AUTHENTICATION");
			try {
				UserDetails userDetails = getUserDbDetails(authCon, user.getUserid());
				if(userDetails == null) {
					logger.error("User Entry not found in DB!!");
					errorList.append("User Entry not found in DB!!");
					throw new RuntimeException(errorList.toString());
				}
				
				logger.info("User db connection url: "+"jdbc:oracle:thin:@"+userDetails.getUrl().trim()+":"+userDetails.getPort().trim()+":"+userDetails.getSid().trim());
				userCon =getConnectionUserDb(userDetails.getUserName(), userDetails.getPwd(),"jdbc:oracle:thin:@"+userDetails.getUrl().trim()+":"+userDetails.getPort().trim()+"/"+userDetails.getSid().trim());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				logger.error("Database Credentials provided are wrong!!");
				errorList.append("Database Credentials provided are wrong!!");
				throw e1;
			}
			
			appCon=pool.getConnectionfromPool("APPLICATION");
			
			result=validateProjectTable(zyRequest,userCon); 
			if(!result.equals("true")){
				errorList.append(result);
				throw new RuntimeException(errorList.toString());

			}
			
			result =validatePrimaryId(zyRequest,userCon);
			if(!result.equals("true"))errorList.append(result+"\n");
			
			result=validateColumnMappings(zyRequest,userCon,appCon);
			if(!result.equals("true"))errorList.append(result+"\n");

			result=validateTagsFields(zyRequest,appCon);

			if(!result.equals("true"))errorList.append(result+"\n");
			
			result =validateWhereCondition(zyRequest,userCon);

			if(!result.equals("true"))errorList.append(result+"\n");
		} catch (Exception e1) {
			e1.printStackTrace();
			
		}finally{
			try {
				if(authCon!=null&&!authCon.isClosed())
					authCon.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
					
			try {
				if(appCon!=null&&!appCon.isClosed())
					appCon.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if(userCon!=null&&!userCon.isClosed())
					userCon.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			
			
			if(errorList.length()>0)
				throw new RuntimeException(errorList.toString());
		}
		

		
		
		StatusTableDAO statusDao =new StatusTableDAO();
		boolean validJobName=false;
		try {
			validJobName=statusDao.validateJobname(zyRequest.getDisplayName(),zyRequest.getUserName());
			if(!validJobName){
				statusDao.removeJob(zyRequest.getDisplayName());
/*				logger.error("ZYRICH JOB CREATION FAILED, UNIQUE JOB NAME REQUIRED!");
				throw new RuntimeException("ZYRICH JOB CREATION FAILED, UNIQUE JOB NAME REQUIRED!");
*/			}
		} catch (ZyrichException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		
		
		
		
		return user;
		
	}
	
	public String validatePrimaryId(ZyrichRequest request,Connection conUser) throws SQLException{
		
		try {
			PreparedStatement st = conUser.prepareStatement("select count(1) from cols where table_name=? AND column_name=?");
			st.setString(1,request.getUserProjectTable().toUpperCase().trim());
			st.setString(2,request.getPrimeryKey().toUpperCase().trim());
			
			ResultSet rs = st.executeQuery();
			if(rs.next()){
				if(rs.getInt(1)==0)
				return "ZY_PRIMARY_KEY :"+request.getPrimeryKey().toUpperCase()+" does not exist !";
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		
		return "true";
	}
	
	public String validateTagsFields(ZyrichRequest request,Connection conApp ) throws SQLException{
		
		try {
			PreparedStatement st = conApp.prepareStatement("select tagname,fields from zyrich_tags_OPS where indexname=?");
			st.setString(1,request.getRepository());
			Map<String,List<String>> tags = new HashMap();
			String tagsNotPresent="";
			ResultSet rs = st.executeQuery();
				Clob clob = null;
				List<String> list = null;
			
				while (rs.next()) {
					clob = rs.getClob(2);
					list = clobToString(clob);
					tags.put(rs.getString(1),  list);
		}
				
			String tagsRequest = request.getModules()==null?"":request.getModules();
			logger.info("tagsRequest : "+tagsRequest);
			String tagsYml[] = tagsRequest.split(",");
				
			for(int i=0;i<tagsYml.length;i++){
				
				if(!tagsYml[i].trim().equals("")&&!tags.containsKey(tagsYml[i])){
					tagsNotPresent+=tagsYml[i]+" ";
				}
			}
				
			if(tagsNotPresent.length()!=0)return "These tags dont exist: "+tagsNotPresent+" ! ";
			
			StringBuilder sb = new StringBuilder();
			String fieldsRequest = request.getFields()==null?"":request.getFields();
			String fields[] = fieldsRequest.split(",");
			
			for(int i=0;i<fields.length;i++){
				if(fields[i].trim().equals(""));
				else sb.append("'"+fields[i].toUpperCase().trim()+"',");
			}
			
			if(sb.length()==0)return "true";
			
			if(sb.length()>0) sb.deleteCharAt(sb.length()-1);
			
			logger.info("select count(1) from cols where table_name=( Select ZIC_TABLENAME FROM ZYRICH4_INDEX_CONFIG WHERE ZIC_INDEXNAME= ? ) AND column_name IN ("+sb.toString()+")");
//			PreparedStatement st2 = conApp.prepareStatement("select count(1) from cols where table_name='"+mapDy.get("ZY_REPOSITORY_NAME")+"' AND column_name IN ("+sb.toString()+")");
			PreparedStatement st2 = conApp.prepareStatement("select count(1) from cols where table_name=( Select ZIC_TABLENAME FROM ZYRICH4_INDEX_CONFIG WHERE ZIC_INDEXNAME= ? ) AND column_name IN ("+sb.toString()+")");
			st2.setString(1, request.getRepository().trim());
			
			ResultSet rs2=st2.executeQuery();
			
			if(rs2.next()){
				int cols=rs2.getInt(1);

				if(cols!=fields.length)
					return "In ZY_FIELDS, some columns mentioned donot exist!";			
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
				
		
		return "true";
	}
	
	private List<String> clobToString(Clob data) {
		List<String> list = new ArrayList<String>();
		try {
			Reader reader = data.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);
			String line;
			while (null != (line = br.readLine())) {
				list.add(line);
			}
			br.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return list;
	}
	
	public String validateProjectTable(ZyrichRequest request,Connection userCon) throws SQLException{

		
		
		try {
			PreparedStatement st = userCon.prepareStatement("select tname from tab WHERE tname=?");
			st.setString(1,request.getUserProjectTable().toUpperCase().trim());
			
			ResultSet rs = st.executeQuery();
			
			if(!rs.next())return "ZY_PROJECT_TABLE does not exist ";
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
				
		
		return "true";
	}
	
	
	
	public String validateColumnMappings(ZyrichRequest request,Connection userCon,Connection appCon){

		Set<String> labelSet = new HashSet();
		Set<String> colSet= new HashSet();
		String labels="";
				
			try {
				PreparedStatement st = appCon.prepareStatement("select ZIC_LABLENAME from ZYRICH4_INDEX_CONFIG where ZIC_INDEXNAME=?");
				st.setString(1, request.getRepository().trim());
				ResultSet rsLabel = st.executeQuery();
				
				if(rsLabel.next())labels = rsLabel.getString(1); 
				String labls[] = labels.split(",");
				for(int i=0;i<labls.length;i++){
					labelSet.add(labls[i]);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				String arr[] = request.getReqColumnName().split(",");
				
				if(arr==null||arr.length!=labelSet.size()) return "ZY_COLUMN_MAPPINGS All Columns must be present, if no mapping present use null keyword  for column name !";
//			List<String> cols = new ArrayList();
				for(String unit: arr){
//					logger.info(unit.substring(0, unit.indexOf(":"))+"  ******");
//					logger.info(unit.substring(unit.indexOf(":")+1,unit.length())+" *****");
					if(labelSet.contains(unit.substring(0, unit.indexOf(":")))){
						colSet.add(unit.substring(unit.indexOf(":")+1,unit.length()));
						//					cols.add(unit.substring(unit.indexOf(":")+1,unit.length()));
					}else{
						
						return "In ZY_COLUMN_MAPPINGS, unknown label name given :"+unit.substring(0, unit.indexOf(":")+1);
					}
				}
			} catch (Exception e1) {
				e1.printStackTrace();
				return "In ZY_COLUMN_MAPPINGS, a column label should be followed by ':' ";
			}
			
			try {
			
			StringBuffer sb = new StringBuffer();
			int c=0;
			
			String colArr[]=colSet.toArray(new String[0]);
			for(int i=0;i<colArr.length;i++){
				if(colArr[i].trim().equals("")||colArr[i].trim().equals("null"));
				else
				{
					sb.append("'"+colArr[i].toUpperCase().trim()+"',");
					c++;
				}
			}
			if(sb.length()==0)return "ZY_COLUMN_MAPPING cannot be empty ! ";
			
			sb.deleteCharAt(sb.length()-1);
			
			logger.info("select count(1) from cols where table_name=? AND column_name IN ("+sb.toString()+")");
			PreparedStatement st2 = userCon.prepareStatement("select count(1) from cols where table_name=? AND column_name IN ("+sb.toString()+")");
			st2.setString(1, request.getUserProjectTable().toUpperCase().trim());
			ResultSet res=st2.executeQuery();
			if(res.next()){
				int a = res.getInt(1)
						;
//				logger.info("a is:"+a);
	//			logger.info("c is: "+c);
				if(a!=c)return "In ZY_COLUMN_MAPPINGS, some columns do not exist ! ";
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return "ZY_COLUMN_MAPPINGS not in proper format!";
		}
		
		
		
		return "true";
	}

	public String validateWhereCondition(ZyrichRequest request,Connection userCon) throws SQLException{
		
		
		
		String res="";
		String whereCondition = request.getWhereCondition();
		if(whereCondition==null)return "true";
		Set<String> colSet = new HashSet();
		StringBuilder sb = new StringBuilder();
		
		Set<String> ops = new HashSet();
		ops.add("=");
		ops.add("<>");
		ops.add("LIKE");
		ops.add(">");
		ops.add("<");
		ops.add(">=");
		ops.add("<=");

		
		try {
			if(whereCondition.trim().equals(""))return "true";
			
			if(whereCondition.trim().endsWith("AND"))
				return "ZY_WHERE_CONDITION cannot end with an 'AND' ";
			else if (whereCondition.trim().endsWith("OR"))
				return "ZY_WHERE_CONDITION cannot end with an 'OR' ";

			if(whereCondition.trim().startsWith("AND"))
				return "ZY_WHERE_CONDITION cannot start with an 'AND' ";
			else if (whereCondition.trim().startsWith("OR"))
				return "ZY_WHERE_CONDITION cannot end with an 'OR' ";

//			String arr[] = whereCondition.toUpperCase().split("AND");
			whereCondition=whereCondition.trim().replaceAll("( )+", " ");//replace multile spaces with single space
			List<String> outList = getSeperatedList(whereCondition, null);
			String arr[] = outList.toArray(new String[0]);
//  ( ZYCUS_ID > 1234 ) AND ( ZYCUS_ID < 4444 ) OR ( ZYCUS_ID = 4 )			
//			if(arr==null||arr.length==0)return "IN ZY_WHERE_CONDITION condtions should be separated by and for run: ";
			for(int i=0;i<arr.length;i++){
				arr[i]=arr[i].trim();
				
				if(arr[i].charAt(0)=='('&&arr[i].charAt(arr[i].length()-1)==')'&&arr[i].length()>2){

					String tokens[] = arr[i].substring(1, arr[i].length()-2).trim().split(" ");				
					if(tokens.length!=3) {
						res="IN ZY_WHERE_CONDITION THERE SHOULD BE 3 arguments for each conditon seperated by a single space || ";
						continue;
					}
				
					if(!tokens[0].toUpperCase().trim().equals("ROWNUM"))//exclude rownum
							colSet.add(tokens[0].toUpperCase().trim());
//						sb.append("'"+tokens[0]+"',");
						if(!ops.contains(tokens[1]))res+="ZY_WHERE_CONDITION contains invalid operator "+tokens[1]+"  | ";
					
				}else res+="IN ZY_WHERE_CONDITION, condition should be surrounded by brackets ";
			}
			
			if(res.length()>0) return res;
			//if(colSet.contains()colSet.remove(o)
			for(String temp:colSet.toArray(new String[0])) {
					sb.append("'"+temp.toUpperCase().trim()+"',");
			}
			
			if(sb.length()>0)sb.deleteCharAt(sb.length()-1); 
				else return "true";
			
			logger.info("select count(1) from cols where table_name=? AND column_name IN ("+sb.toString()+")");
			PreparedStatement st2 = userCon.prepareStatement("select count(1) from cols where table_name=? AND column_name IN ("+sb.toString()+")");
			st2.setString(1, request.getUserProjectTable().toUpperCase().trim());
			ResultSet rs=st2.executeQuery();
			if(rs.next()){
				int a = rs.getInt(1);
				logger.info("a is:"+a);
				logger.info("arr length is: "+colSet.size());
				if(a!=(colSet.size()))return res+"In ZY_WHERE_CONDITION, some columns donot exist! ";
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return "ZY_WHERE_CONDITION NOT IN PROPER FORMAT!";
		}

		
		
		return res.equals("")?"true":res;
	}

	
	
	static  List<String> getSeperatedList(String whereCondition,List<String> outList){
		if(outList==null)outList = new ArrayList();
		int andPos=Integer.MAX_VALUE;
		int orPos = Integer.MAX_VALUE;
		
		andPos=whereCondition.indexOf(" AND ");
		orPos=whereCondition.indexOf(" OR ");
		System.out.println(String.format("and = %s , or = %s ,whereCondition = %s ", andPos,orPos,whereCondition));
		if(andPos==-1)andPos=Integer.MAX_VALUE;
		if(orPos==-1)orPos=Integer.MAX_VALUE;
		
		if(andPos<orPos){
			
			outList.add(whereCondition.substring(0, andPos));
			System.out.println("adding "+whereCondition.substring(0, andPos));
			whereCondition=whereCondition.replaceFirst(" AND ", "");
			whereCondition = whereCondition.substring(andPos,whereCondition.length());
			return getSeperatedList(whereCondition,outList);
		}else if(orPos<andPos){

			outList.add(whereCondition.substring(0, orPos));
			System.out.println("adding "+whereCondition.substring(0, orPos));
			whereCondition=whereCondition.replaceFirst(" OR ", "");
			whereCondition = whereCondition.substring(orPos,whereCondition.length());
			return getSeperatedList(whereCondition,outList);

		}else {
			outList.add(whereCondition);
		}
		
		
		
		return outList;
	}
	
	
	public Connection getConnectionUserDb(String username,String password,String connurl) throws Exception{
		Connection connection = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			connection = DriverManager.getConnection(connurl, username,
					password);

		} catch (final ClassNotFoundException e) {
			throw new Exception(e);

		} catch (final SQLException e) {
			throw new Exception(e);
		}

		return connection;
	}

	public UserDetails getUserDbDetails(Connection con,int userId) throws SQLException{
		
		
		UserDetails user =null;
		
		
		try {
			PreparedStatement st = con.prepareStatement("select USERNAME, PASSWORD, URL, PORT, SID from ASA_DBCONFIG where USERID=?");
			st.setInt(1, userId);
			ResultSet rs  = st.executeQuery();
			
			if(rs.next())user = new UserDetails(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), userId, -1);
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		
		
		return user;
	}
	
	
	private class UserDetails{
		
		private String userName;
		private String pwd;
		private String url;
		private String port;
		private String sid;
		private int userId;
		private int projectid;
		
		
		public UserDetails(String userName, String pwd, String url, String port,
				String sid, int userId, int projectid) {
			super();
			this.userName = userName;
			this.pwd = pwd;
			this.url = url;
			this.port = port;
			this.sid = sid;
			this.userId = userId;
			this.projectid = projectid;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getPwd() {
			return pwd;
		}
		public void setPwd(String pwd) {
			this.pwd = pwd;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public String getPort() {
			return port;
		}
		public void setPort(String port) {
			this.port = port;
		}
		public String getSid() {
			return sid;
		}
		public void setSid(String sid) {
			this.sid = sid;
		}
		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}
		public int getProjectid() {
			return projectid;
		}
		public void setProjectid(int projectid) {
			this.projectid = projectid;
		}
	
		
		
	}


}
